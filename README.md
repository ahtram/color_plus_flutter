# color_plus

A convenient color library borrowed from https://www.colorhexa.com/color-names

## Import color_plus

```dart
import 'package:color_plus/color_plus.dart' as ColorPlus;
```

Import the library and use it as a prefix is advised.

## Get any pre-defined color by their name!

```dart
Color myColor = ColorPlus.DeepFuchsia;
```

## Traversal all colors in the library.

```dart
for (int i = 0 ; i < ColorPlus.colors.length ; i++) {
  print(ColorPlus.colors[i].toString());
}
```

## Traversal all color names by the ColorName enum.

```dart
for (int i = 0 ; i < ColorPlus.ColorName.values.length ; i++) {
  print(ColorPlus.ColorName.values[i]);
}
```

## Get a pre-defined color by ColorName enum.

```dart
Color myColor = ColorPlus.getColorByName(ColorPlus.ColorName.DeepFuchsia);
```

## A complete color ticket showcase.

Build the main.dart to see it.

![a screenshot of complete color tickets](http://uni-team.xyz/nextCloud/index.php/s/3prAZ2PFtz6x97c/preview)