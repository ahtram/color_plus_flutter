import 'package:flutter/material.dart';
import 'color_plus.dart' as ColorPlus;

void main() {
  runApp(MaterialApp(
    title: 'Color Plus',
    theme: ThemeData(
      // This is the theme of your application.
      //
      // Try running your application with "flutter run". You'll see the
      // application has a blue toolbar. Then, without quitting the app, try
      // changing the primarySwatch below to Colors.green and then invoke
      // "hot reload" (press "r" in the console where you ran "flutter run",
      // or simply save your changes to "hot reload" in a Flutter IDE).
      // Notice that the counter didn't reset back to zero; the application
      // is not restarted.
      primarySwatch: Colors.blue,
      // This makes the visual density adapt to the platform that you run
      // the app on. For desktop platforms, the controls will be smaller and
      // closer together (more dense) than on mobile platforms.
      visualDensity: VisualDensity.adaptivePlatformDensity,
    ),
    home: ColorPlusTester(),
  ));
}

class ColorPlusTester extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      primary: false,
      appBar: null,
      body: ListView(
        children: ColorPlus.ColorName.values.map(
            (colorNameEnum) => ColorTicket(
              color: ColorPlus.colors[ColorPlus.ColorName.values.indexOf(colorNameEnum)],
              name: colorNameEnum.toString().substring(colorNameEnum.toString().indexOf('.') + 1),
            )
        ).toList(),
      ),
    );
  }
}

//A simple widget for display a color ticket.
class ColorTicket extends StatelessWidget {
  ColorTicket({required this.color, required this.name, this.height = 30});

  final String name;
  final Color color;
  final double height;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      color: color,
      child: Center(
        child: Text(
          color.toString(),
          style: TextStyle(
              color: color.suitableTextColor(),
            fontSize: 16,
          ),
        ),
      ),
    );
  }
}