import 'dart:ui';
import 'package:flutter/material.dart';
import 'dart:math';

extension ColorMagic on Color {
  // Calculate the average rgb value of a color.
  double rgbAverage() {
    return (this.red + this.green + this.blue) / 3.0;
  }

  // Get the inverted color.
  Color invertedColor() {
    return Color.fromRGBO(
        255 - this.red, 255 - this.green, 255 - this.blue, 1.0);
  }

  // Get suitable black or white color for text display.
  Color suitableTextColor() {
    if (this.rgbAverage() > 127.5) {
      return Colors.black;
    } else {
      return Colors.white;
    }
  }
}

// The a color by enum value.
Color getColorByName(ColorName colorName) {
  return colors[ColorName.values.indexOf(colorName)];
}

// Get a random color from the library.
Color getRandomColor() {
  return colors[Random().nextInt(colors.length)];
}

// Lots of color names.
// See https://www.colorhexa.com/color-names
enum ColorName {
  AirForceBlue,
  AliceBlue,
  AlizarinCrimson,
  Almond,
  Amaranth,
  Amber,
  AmericanRose,
  Amethyst,
  AndroidGreen,
  AntiFlashWhite,
  AntiqueBrass,
  AntiqueFuchsia,
  AntiqueWhite,
  Ao,
  AppleGreen,
  Apricot,
  Aqua,
  Aquamarine,
  ArmyGreen,
  ArylideYellow,
  AshGrey,
  Asparagus,
  AtomicTangerine,
  Auburn,
  Aureolin,
  Aurometalsaurus,
  Awesome,
  Azure,
  AzureMist,
  BabyBlue,
  BabyBlueEyes,
  BabyPink,
  BallBlue,
  BananaMania,
  BananaYellow,
  BattleshipGrey,
  Bazaar,
  BeauBlue,
  Beaver,
  Beige,
  Bisque,
  Bistre,
  Bittersweet,
  Black,
  BlanchedAlmond,
  BleuDeFrance,
  BlizzardBlue,
  Blond,
  Blue,
  BlueBell,
  BlueGray,
  BlueGreen,
  BluePurple,
  BlueViolet,
  Blush,
  Bole,
  BondiBlue,
  Bone,
  BostonUniversityRed,
  BottleGreen,
  Boysenberry,
  BrandeisBlue,
  Brass,
  BrickRed,
  BrightCerulean,
  BrightGreen,
  BrightLavender,
  BrightMaroon,
  BrightPink,
  BrightTurquoise,
  BrightUbe,
  BrilliantLavender,
  BrilliantRose,
  BrinkPink,
  BritishRacingGreen,
  Bronze,
  Brown,
  BubbleGum,
  Bubbles,
  Buff,
  BulgarianRose,
  Burgundy,
  Burlywood,
  BurntOrange,
  BurntSienna,
  BurntUmber,
  Byzantine,
  Byzantium,
  CgBlue,
  CgRed,
  Cadet,
  CadetBlue,
  CadetGrey,
  CadmiumGreen,
  CadmiumOrange,
  CadmiumRed,
  CadmiumYellow,
  CafeAuLait,
  CafeNoir,
  CalPolyPomonaGreen,
  CambridgeBlue,
  Camel,
  CamouflageGreen,
  Canary,
  CanaryYellow,
  CandyAppleRed,
  CandyPink,
  Capri,
  CaputMortuum,
  Cardinal,
  CaribbeanGreen,
  Carmine,
  CarminePink,
  CarmineRed,
  CarnationPink,
  Carnelian,
  CarolinaBlue,
  CarrotOrange,
  Celadon,
  Celeste,
  CelestialBlue,
  Cerise,
  CerisePink,
  Cerulean,
  CeruleanBlue,
  Chamoisee,
  Champagne,
  Charcoal,
  Chartreuse,
  Cherry,
  CherryBlossomPink,
  Chestnut,
  Chocolate,
  ChromeYellow,
  Cinereous,
  Cinnabar,
  Cinnamon,
  Citrine,
  ClassicRose,
  Cobalt,
  CocoaBrown,
  Coffee,
  ColumbiaBlue,
  CoolBlack,
  CoolGrey,
  Copper,
  CopperRose,
  Coquelicot,
  Coral,
  CoralPink,
  CoralRed,
  Cordovan,
  Corn,
  CornellRed,
  Cornflower,
  CornflowerBlue,
  Cornsilk,
  CosmicLatte,
  CottonCandy,
  Cream,
  Crimson,
  CrimsonRed,
  CrimsonGlory,
  Cyan,
  Daffodil,
  Dandelion,
  DarkBlue,
  DarkBrown,
  DarkByzantium,
  DarkCandyAppleRed,
  DarkCerulean,
  DarkChestnut,
  DarkCoral,
  DarkCyan,
  DarkElectricBlue,
  DarkGoldenrod,
  DarkGray,
  DarkGreen,
  DarkJungleGreen,
  DarkKhaki,
  DarkLava,
  DarkLavender,
  DarkMagenta,
  DarkMidnightBlue,
  DarkOliveGreen,
  DarkOrange,
  DarkOrchid,
  DarkPastelBlue,
  DarkPastelGreen,
  DarkPastelPurple,
  DarkPastelRed,
  DarkPink,
  DarkPowderBlue,
  DarkRaspberry,
  DarkRed,
  DarkSalmon,
  DarkScarlet,
  DarkSeaGreen,
  DarkSienna,
  DarkSlateBlue,
  DarkSlateGray,
  DarkSpringGreen,
  DarkTan,
  DarkTangerine,
  DarkTaupe,
  DarkTerraCotta,
  DarkTurquoise,
  DarkViolet,
  DartmouthGreen,
  DavyGrey,
  DebianRed,
  DeepCarmine,
  DeepCarminePink,
  DeepCarrotOrange,
  DeepCerise,
  DeepChampagne,
  DeepChestnut,
  DeepCoffee,
  DeepFuchsia,
  DeepJungleGreen,
  DeepLilac,
  DeepMagenta,
  DeepPeach,
  DeepPink,
  DeepSaffron,
  DeepSkyBlue,
  Denim,
  Desert,
  DesertSand,
  DimGray,
  DodgerBlue,
  DogwoodRose,
  DollarBill,
  Drab,
  DukeBlue,
  EarthYellow,
  Ecru,
  Eggplant,
  Eggshell,
  EgyptianBlue,
  ElectricBlue,
  ElectricCrimson,
  ElectricCyan,
  ElectricGreen,
  ElectricIndigo,
  ElectricLavender,
  ElectricLime,
  ElectricPurple,
  ElectricUltramarine,
  ElectricViolet,
  ElectricYellow,
  Emerald,
  EtonBlue,
  Fallow,
  FaluRed,
  Famous,
  Fandango,
  FashionFuchsia,
  Fawn,
  Feldgrau,
  Fern,
  FernGreen,
  FerrariRed,
  FieldDrab,
  FireEngineRed,
  Firebrick,
  Flame,
  FlamingoPink,
  Flavescent,
  Flax,
  FloralWhite,
  FluorescentOrange,
  FluorescentPink,
  FluorescentYellow,
  Folly,
  ForestGreen,
  FrenchBeige,
  FrenchBlue,
  FrenchLilac,
  FrenchRose,
  Fuchsia,
  FuchsiaPink,
  Fulvous,
  FuzzyWuzzy,
  Gainsboro,
  Gamboge,
  GhostWhite,
  Ginger,
  Glaucous,
  Glitter,
  Gold,
  GoldenBrown,
  GoldenPoppy,
  GoldenYellow,
  Goldenrod,
  GrannySmithApple,
  Gray,
  GrayAsparagus,
  Green,
  GreenBlue,
  GreenYellow,
  Grullo,
  GuppieGreen,
  HalayaUbe,
  HanBlue,
  HanPurple,
  HansaYellow,
  Harlequin,
  HarvardCrimson,
  HarvestGold,
  HeartGold,
  Heliotrope,
  HollywoodCerise,
  Honeydew,
  HookerGreen,
  HotMagenta,
  HotPink,
  HunterGreen,
  Icterine,
  Inchworm,
  IndiaGreen,
  IndianRed,
  IndianYellow,
  Indigo,
  InternationalKleinBlue,
  InternationalOrange,
  Iris,
  Isabelline,
  IslamicGreen,
  Ivory,
  Jade,
  Jasmine,
  Jasper,
  JazzberryJam,
  Jonquil,
  JuneBud,
  JungleGreen,
  KuCrimson,
  KellyGreen,
  Khaki,
  LaSalleGreen,
  LanguidLavender,
  LapisLazuli,
  LaserLemon,
  LaurelGreen,
  Lava,
  Lavender,
  LavenderBlue,
  LavenderBlush,
  LavenderGray,
  LavenderIndigo,
  LavenderMagenta,
  LavenderMist,
  LavenderPink,
  LavenderPurple,
  LavenderRose,
  LawnGreen,
  Lemon,
  LemonYellow,
  LemonChiffon,
  LemonLime,
  LightCrimson,
  LightThulianPink,
  LightApricot,
  LightBlue,
  LightBrown,
  LightCarminePink,
  LightCoral,
  LightCornflowerBlue,
  LightCyan,
  LightFuchsiaPink,
  LightGoldenrodYellow,
  LightGray,
  LightGreen,
  LightKhaki,
  LightPastelPurple,
  LightPink,
  LightSalmon,
  LightSalmonPink,
  LightSeaGreen,
  LightSkyBlue,
  LightSlateGray,
  LightTaupe,
  LightYellow,
  Lilac,
  Lime,
  LimeGreen,
  LincolnGreen,
  Linen,
  Lion,
  Liver,
  Lust,
  MsuGreen,
  MacaroniAndCheese,
  Magenta,
  MagicMint,
  Magnolia,
  Mahogany,
  Maize,
  MajorelleBlue,
  Malachite,
  Manatee,
  MangoTango,
  Mantis,
  Maroon,
  Mauve,
  MauveTaupe,
  Mauvelous,
  MayaBlue,
  MeatBrown,
  MediumPersianBlue,
  MediumAquamarine,
  MediumBlue,
  MediumCandyAppleRed,
  MediumCarmine,
  MediumChampagne,
  MediumElectricBlue,
  MediumJungleGreen,
  MediumLavenderMagenta,
  MediumOrchid,
  MediumPurple,
  MediumRedViolet,
  MediumSeaGreen,
  MediumSlateBlue,
  MediumSpringBud,
  MediumSpringGreen,
  MediumTaupe,
  MediumTealBlue,
  MediumTurquoise,
  MediumVioletRed,
  Melon,
  MidnightBlue,
  MidnightGreen,
  MikadoYellow,
  Mint,
  MintCream,
  MintGreen,
  MistyRose,
  Moccasin,
  ModeBeige,
  MoonstoneBlue,
  MordantRed19,
  MossGreen,
  MountainMeadow,
  MountbattenPink,
  Mulberry,
  Munsell,
  Mustard,
  Myrtle,
  NadeshikoPink,
  NapierGreen,
  NaplesYellow,
  NavajoWhite,
  NavyBlue,
  NeonCarrot,
  NeonFuchsia,
  NeonGreen,
  NonPhotoBlue,
  NorthTexasGreen,
  OceanBoatBlue,
  Ochre,
  OfficeGreen,
  OldGold,
  OldLace,
  OldLavender,
  OldMauve,
  OldRose,
  Olive,
  OliveDrab,
  OliveGreen,
  Olivine,
  Onyx,
  OperaMauve,
  Orange,
  OrangeYellow,
  OrangePeel,
  OrangeRed,
  Orchid,
  OtterBrown,
  OuterSpace,
  OutrageousOrange,
  OxfordBlue,
  PacificBlue,
  PakistanGreen,
  PalatinateBlue,
  PalatinatePurple,
  PaleAqua,
  PaleBlue,
  PaleBrown,
  PaleCarmine,
  PaleCerulean,
  PaleChestnut,
  PaleCopper,
  PaleCornflowerBlue,
  PaleGold,
  PaleGoldenrod,
  PaleGreen,
  PaleLavender,
  PaleMagenta,
  PalePink,
  PalePlum,
  PaleRedViolet,
  PaleRobinEggBlue,
  PaleSilver,
  PaleSpringBud,
  PaleTaupe,
  PaleVioletRed,
  PansyPurple,
  PapayaWhip,
  ParisGreen,
  PastelBlue,
  PastelBrown,
  PastelGray,
  PastelGreen,
  PastelMagenta,
  PastelOrange,
  PastelPink,
  PastelPurple,
  PastelRed,
  PastelViolet,
  PastelYellow,
  Patriarch,
  PayneGrey,
  Peach,
  PeachPuff,
  PeachYellow,
  Pear,
  Pearl,
  PearlAqua,
  Peridot,
  Periwinkle,
  PersianBlue,
  PersianIndigo,
  PersianOrange,
  PersianPink,
  PersianPlum,
  PersianRed,
  PersianRose,
  Phlox,
  PhthaloBlue,
  PhthaloGreen,
  PiggyPink,
  PineGreen,
  Pink,
  PinkFlamingo,
  PinkSherbet,
  PinkPearl,
  Pistachio,
  Platinum,
  Plum,
  PortlandOrange,
  PowderBlue,
  PrincetonOrange,
  PrussianBlue,
  PsychedelicPurple,
  Puce,
  Pumpkin,
  Purple,
  PurpleHeart,
  PurpleMountainMajesty,
  PurpleMountainsMajesty,
  PurplePizzazz,
  PurpleTaupe,
  Rackley,
  RadicalRed,
  Raspberry,
  RaspberryGlace,
  RaspberryPink,
  RaspberryRose,
  RawSienna,
  RazzleDazzleRose,
  Razzmatazz,
  Red,
  RedOrange,
  RedBrown,
  RedViolet,
  RichBlack,
  RichCarmine,
  RichElectricBlue,
  RichLilac,
  RichMaroon,
  RifleGreen,
  RobinsEggBlue,
  Rose,
  RoseBonbon,
  RoseEbony,
  RoseGold,
  RoseMadder,
  RosePink,
  RoseQuartz,
  RoseTaupe,
  RoseVale,
  Rosewood,
  RossoCorsa,
  RosyBrown,
  RoyalAzure,
  RoyalBlue,
  RoyalFuchsia,
  RoyalPurple,
  Ruby,
  Ruddy,
  RuddyBrown,
  RuddyPink,
  Rufous,
  Russet,
  Rust,
  SacramentoStateGreen,
  SaddleBrown,
  SafetyOrange,
  Saffron,
  SaintPatrickBlue,
  Salmon,
  SalmonPink,
  Sand,
  SandDune,
  Sandstorm,
  SandyBrown,
  SandyTaupe,
  SapGreen,
  Sapphire,
  SatinSheenGold,
  Scarlet,
  SchoolBusYellow,
  ScreaminGreen,
  SeaBlue,
  SeaGreen,
  SealBrown,
  Seashell,
  SelectiveYellow,
  Sepia,
  Shadow,
  Shamrock,
  ShamrockGreen,
  ShockingPink,
  Sienna,
  Silver,
  Sinopia,
  Skobeloff,
  SkyBlue,
  SkyMagenta,
  SlateBlue,
  SlateGray,
  Smalt,
  SmokeyTopaz,
  SmokyBlack,
  Snow,
  SpiroDiscoBall,
  SpringBud,
  SpringGreen,
  SteelBlue,
  StilDeGrainYellow,
  Stizza,
  Stormcloud,
  Straw,
  Sunglow,
  Sunset,
  SunsetOrange,
  Tan,
  Tangelo,
  Tangerine,
  TangerineYellow,
  Taupe,
  TaupeGray,
  Tawny,
  TeaGreen,
  TeaRose,
  Teal,
  TealBlue,
  TealGreen,
  TerraCotta,
  Thistle,
  ThulianPink,
  TickleMePink,
  TiffanyBlue,
  TigerEye,
  Timberwolf,
  TitaniumYellow,
  Tomato,
  Toolbox,
  Topaz,
  TractorRed,
  TrolleyGrey,
  TropicalRainForest,
  TrueBlue,
  TuftsBlue,
  Tumbleweed,
  TurkishRose,
  Turquoise,
  TurquoiseBlue,
  TurquoiseGreen,
  TuscanRed,
  TwilightLavender,
  TyrianPurple,
  UaBlue,
  UaRed,
  UclaBlue,
  UclaGold,
  UfoGreen,
  UpForestGreen,
  UpMaroon,
  UscCardinal,
  UscGold,
  Ube,
  UltraPink,
  Ultramarine,
  UltramarineBlue,
  Umber,
  UnitedNationsBlue,
  UniversityOfCaliforniaGold,
  UnmellowYellow,
  UpsdellRed,
  Urobilin,
  UtahCrimson,
  Vanilla,
  VegasGold,
  VenetianRed,
  Verdigris,
  Vermilion,
  Veronica,
  Violet,
  VioletBlue,
  VioletRed,
  Viridian,
  VividAuburn,
  VividBurgundy,
  VividCerise,
  VividTangerine,
  VividViolet,
  WarmBlack,
  Waterspout,
  Wenge,
  Wheat,
  White,
  WhiteSmoke,
  WildStrawberry,
  WildWatermelon,
  WildBlueYonder,
  Wine,
  Wisteria,
  Xanadu,
  YaleBlue,
  Yellow,
  YellowOrange,
  YellowGreen,
  Zaffre,
  ZinnwalditeBrown,
}

// Lots of colors define.
const Color AirForceBlue = Color(0xff5d8aa8);
const Color AliceBlue = Color(0xfff0f8ff);
const Color AlizarinCrimson = Color(0xffe32636);
const Color Almond = Color(0xffefdecd);
const Color Amaranth = Color(0xffe52b50);
const Color Amber = Color(0xffffbf00);
const Color AmericanRose = Color(0xffff033e);
const Color Amethyst = Color(0xff9966cc);
const Color AndroidGreen = Color(0xffa4c639);
const Color AntiFlashWhite = Color(0xfff2f3f4);
const Color AntiqueBrass = Color(0xffcd9575);
const Color AntiqueFuchsia = Color(0xff915c83);
const Color AntiqueWhite = Color(0xfffaebd7);
const Color Ao = Color(0xff008000);
const Color AppleGreen = Color(0xff8db600);
const Color Apricot = Color(0xfffbceb1);
const Color Aqua = Color(0xff00ffff);
const Color Aquamarine = Color(0xff7fffd4);
const Color ArmyGreen = Color(0xff4b5320);
const Color ArylideYellow = Color(0xffe9d66b);
const Color AshGrey = Color(0xffb2beb5);
const Color Asparagus = Color(0xff87a96b);
const Color AtomicTangerine = Color(0xffff9966);
const Color Auburn = Color(0xffa52a2a);
const Color Aureolin = Color(0xfffdee00);
const Color Aurometalsaurus = Color(0xff6e7f80);
const Color Awesome = Color(0xffff2052);
const Color Azure = Color(0xff007fff);
const Color AzureMist = Color(0xfff0ffff);
const Color BabyBlue = Color(0xff89cff0);
const Color BabyBlueEyes = Color(0xffa1caf1);
const Color BabyPink = Color(0xfff4c2c2);
const Color BallBlue = Color(0xff21abcd);
const Color BananaMania = Color(0xfffae7b5);
const Color BananaYellow = Color(0xffffe135);
const Color BattleshipGrey = Color(0xff848482);
const Color Bazaar = Color(0xff98777b);
const Color BeauBlue = Color(0xffbcd4e6);
const Color Beaver = Color(0xff9f8170);
const Color Beige = Color(0xfff5f5dc);
const Color Bisque = Color(0xffffe4c4);
const Color Bistre = Color(0xff3d2b1f);
const Color Bittersweet = Color(0xfffe6f5e);
const Color Black = Color(0xff000000);
const Color BlanchedAlmond = Color(0xffffebcd);
const Color BleuDeFrance = Color(0xff318ce7);
const Color BlizzardBlue = Color(0xfface5ee);
const Color Blond = Color(0xfffaf0be);
const Color Blue = Color(0xff0000ff);
const Color BlueBell = Color(0xffa2a2d0);
const Color BlueGray = Color(0xff6699cc);
const Color BlueGreen = Color(0xff0d98ba);
const Color BluePurple = Color(0xff8a2be2);
const Color BlueViolet = Color(0xff8a2be2);
const Color Blush = Color(0xffde5d83);
const Color Bole = Color(0xff79443b);
const Color BondiBlue = Color(0xff0095b6);
const Color Bone = Color(0xffe3dac9);
const Color BostonUniversityRed = Color(0xffcc0000);
const Color BottleGreen = Color(0xff006a4e);
const Color Boysenberry = Color(0xff873260);
const Color BrandeisBlue = Color(0xff0070ff);
const Color Brass = Color(0xffb5a642);
const Color BrickRed = Color(0xffcb4154);
const Color BrightCerulean = Color(0xff1dacd6);
const Color BrightGreen = Color(0xff66ff00);
const Color BrightLavender = Color(0xffbf94e4);
const Color BrightMaroon = Color(0xffc32148);
const Color BrightPink = Color(0xffff007f);
const Color BrightTurquoise = Color(0xff08e8de);
const Color BrightUbe = Color(0xffd19fe8);
const Color BrilliantLavender = Color(0xfff4bbff);
const Color BrilliantRose = Color(0xffff55a3);
const Color BrinkPink = Color(0xfffb607f);
const Color BritishRacingGreen = Color(0xff004225);
const Color Bronze = Color(0xffcd7f32);
const Color Brown = Color(0xffa52a2a);
const Color BubbleGum = Color(0xffffc1cc);
const Color Bubbles = Color(0xffe7feff);
const Color Buff = Color(0xfff0dc82);
const Color BulgarianRose = Color(0xff480607);
const Color Burgundy = Color(0xff800020);
const Color Burlywood = Color(0xffdeb887);
const Color BurntOrange = Color(0xffcc5500);
const Color BurntSienna = Color(0xffe97451);
const Color BurntUmber = Color(0xff8a3324);
const Color Byzantine = Color(0xffbd33a4);
const Color Byzantium = Color(0xff702963);
const Color CgBlue = Color(0xff007aa5);
const Color CgRed = Color(0xffe03c31);
const Color Cadet = Color(0xff536872);
const Color CadetBlue = Color(0xff5f9ea0);
const Color CadetGrey = Color(0xff91a3b0);
const Color CadmiumGreen = Color(0xff006b3c);
const Color CadmiumOrange = Color(0xffed872d);
const Color CadmiumRed = Color(0xffe30022);
const Color CadmiumYellow = Color(0xfffff600);
const Color CafeAuLait = Color(0xffa67b5b);
const Color CafeNoir = Color(0xff4b3621);
const Color CalPolyPomonaGreen = Color(0xff1e4d2b);
const Color CambridgeBlue = Color(0xffa3c1ad);
const Color Camel = Color(0xffc19a6b);
const Color CamouflageGreen = Color(0xff78866b);
const Color Canary = Color(0xffffff99);
const Color CanaryYellow = Color(0xffffef00);
const Color CandyAppleRed = Color(0xffff0800);
const Color CandyPink = Color(0xffe4717a);
const Color Capri = Color(0xff00bfff);
const Color CaputMortuum = Color(0xff592720);
const Color Cardinal = Color(0xffc41e3a);
const Color CaribbeanGreen = Color(0xff00cc99);
const Color Carmine = Color(0xffff0040);
const Color CarminePink = Color(0xffeb4c42);
const Color CarmineRed = Color(0xffff0038);
const Color CarnationPink = Color(0xffffa6c9);
const Color Carnelian = Color(0xffb31b1b);
const Color CarolinaBlue = Color(0xff99badd);
const Color CarrotOrange = Color(0xffed9121);
const Color Celadon = Color(0xfface1af);
const Color Celeste = Color(0xffb2ffff);
const Color CelestialBlue = Color(0xff4997d0);
const Color Cerise = Color(0xffde3163);
const Color CerisePink = Color(0xffec3b83);
const Color Cerulean = Color(0xff007ba7);
const Color CeruleanBlue = Color(0xff2a52be);
const Color Chamoisee = Color(0xffa0785a);
const Color Champagne = Color(0xfffad6a5);
const Color Charcoal = Color(0xff36454f);
const Color Chartreuse = Color(0xff7fff00);
const Color Cherry = Color(0xffde3163);
const Color CherryBlossomPink = Color(0xffffb7c5);
const Color Chestnut = Color(0xffcd5c5c);
const Color Chocolate = Color(0xffd2691e);
const Color ChromeYellow = Color(0xffffa700);
const Color Cinereous = Color(0xff98817b);
const Color Cinnabar = Color(0xffe34234);
const Color Cinnamon = Color(0xffd2691e);
const Color Citrine = Color(0xffe4d00a);
const Color ClassicRose = Color(0xfffbcce7);
const Color Cobalt = Color(0xff0047ab);
const Color CocoaBrown = Color(0xffd2691e);
const Color Coffee = Color(0xff6f4e37);
const Color ColumbiaBlue = Color(0xff9bddff);
const Color CoolBlack = Color(0xff002e63);
const Color CoolGrey = Color(0xff8c92ac);
const Color Copper = Color(0xffb87333);
const Color CopperRose = Color(0xff996666);
const Color Coquelicot = Color(0xffff3800);
const Color Coral = Color(0xffff7f50);
const Color CoralPink = Color(0xfff88379);
const Color CoralRed = Color(0xffff4040);
const Color Cordovan = Color(0xff893f45);
const Color Corn = Color(0xfffbec5d);
const Color CornellRed = Color(0xffb31b1b);
const Color Cornflower = Color(0xff9aceeb);
const Color CornflowerBlue = Color(0xff6495ed);
const Color Cornsilk = Color(0xfffff8dc);
const Color CosmicLatte = Color(0xfffff8e7);
const Color CottonCandy = Color(0xffffbcd9);
const Color Cream = Color(0xfffffdd0);
const Color Crimson = Color(0xffdc143c);
const Color CrimsonRed = Color(0xff990000);
const Color CrimsonGlory = Color(0xffbe0032);
const Color Cyan = Color(0xff00ffff);
const Color Daffodil = Color(0xffffff31);
const Color Dandelion = Color(0xfff0e130);
const Color DarkBlue = Color(0xff00008b);
const Color DarkBrown = Color(0xff654321);
const Color DarkByzantium = Color(0xff5d3954);
const Color DarkCandyAppleRed = Color(0xffa40000);
const Color DarkCerulean = Color(0xff08457e);
const Color DarkChestnut = Color(0xff986960);
const Color DarkCoral = Color(0xffcd5b45);
const Color DarkCyan = Color(0xff008b8b);
const Color DarkElectricBlue = Color(0xff536878);
const Color DarkGoldenrod = Color(0xffb8860b);
const Color DarkGray = Color(0xffa9a9a9);
const Color DarkGreen = Color(0xff013220);
const Color DarkJungleGreen = Color(0xff1a2421);
const Color DarkKhaki = Color(0xffbdb76b);
const Color DarkLava = Color(0xff483c32);
const Color DarkLavender = Color(0xff734f96);
const Color DarkMagenta = Color(0xff8b008b);
const Color DarkMidnightBlue = Color(0xff003366);
const Color DarkOliveGreen = Color(0xff556b2f);
const Color DarkOrange = Color(0xffff8c00);
const Color DarkOrchid = Color(0xff9932cc);
const Color DarkPastelBlue = Color(0xff779ecb);
const Color DarkPastelGreen = Color(0xff03c03c);
const Color DarkPastelPurple = Color(0xff966fd6);
const Color DarkPastelRed = Color(0xffc23b22);
const Color DarkPink = Color(0xffe75480);
const Color DarkPowderBlue = Color(0xff003399);
const Color DarkRaspberry = Color(0xff872657);
const Color DarkRed = Color(0xff8b0000);
const Color DarkSalmon = Color(0xffe9967a);
const Color DarkScarlet = Color(0xff560319);
const Color DarkSeaGreen = Color(0xff8fbc8f);
const Color DarkSienna = Color(0xff3c1414);
const Color DarkSlateBlue = Color(0xff483d8b);
const Color DarkSlateGray = Color(0xff2f4f4f);
const Color DarkSpringGreen = Color(0xff177245);
const Color DarkTan = Color(0xff918151);
const Color DarkTangerine = Color(0xffffa812);
const Color DarkTaupe = Color(0xff483c32);
const Color DarkTerraCotta = Color(0xffcc4e5c);
const Color DarkTurquoise = Color(0xff00ced1);
const Color DarkViolet = Color(0xff9400d3);
const Color DartmouthGreen = Color(0xff00693e);
const Color DavyGrey = Color(0xff555555);
const Color DebianRed = Color(0xffd70a53);
const Color DeepCarmine = Color(0xffa9203e);
const Color DeepCarminePink = Color(0xffef3038);
const Color DeepCarrotOrange = Color(0xffe9692c);
const Color DeepCerise = Color(0xffda3287);
const Color DeepChampagne = Color(0xfffad6a5);
const Color DeepChestnut = Color(0xffb94e48);
const Color DeepCoffee = Color(0xff704241);
const Color DeepFuchsia = Color(0xffc154c1);
const Color DeepJungleGreen = Color(0xff004b49);
const Color DeepLilac = Color(0xff9955bb);
const Color DeepMagenta = Color(0xffcc00cc);
const Color DeepPeach = Color(0xffffcba4);
const Color DeepPink = Color(0xffff1493);
const Color DeepSaffron = Color(0xffff9933);
const Color DeepSkyBlue = Color(0xff00bfff);
const Color Denim = Color(0xff1560bd);
const Color Desert = Color(0xffc19a6b);
const Color DesertSand = Color(0xffedc9af);
const Color DimGray = Color(0xff696969);
const Color DodgerBlue = Color(0xff1e90ff);
const Color DogwoodRose = Color(0xffd71868);
const Color DollarBill = Color(0xff85bb65);
const Color Drab = Color(0xff967117);
const Color DukeBlue = Color(0xff00009c);
const Color EarthYellow = Color(0xffe1a95f);
const Color Ecru = Color(0xffc2b280);
const Color Eggplant = Color(0xff614051);
const Color Eggshell = Color(0xfff0ead6);
const Color EgyptianBlue = Color(0xff1034a6);
const Color ElectricBlue = Color(0xff7df9ff);
const Color ElectricCrimson = Color(0xffff003f);
const Color ElectricCyan = Color(0xff00ffff);
const Color ElectricGreen = Color(0xff00ff00);
const Color ElectricIndigo = Color(0xff6f00ff);
const Color ElectricLavender = Color(0xfff4bbff);
const Color ElectricLime = Color(0xffccff00);
const Color ElectricPurple = Color(0xffbf00ff);
const Color ElectricUltramarine = Color(0xff3f00ff);
const Color ElectricViolet = Color(0xff8f00ff);
const Color ElectricYellow = Color(0xffffff00);
const Color Emerald = Color(0xff50c878);
const Color EtonBlue = Color(0xff96c8a2);
const Color Fallow = Color(0xffc19a6b);
const Color FaluRed = Color(0xff801818);
const Color Famous = Color(0xffff00ff);
const Color Fandango = Color(0xffb53389);
const Color FashionFuchsia = Color(0xfff400a1);
const Color Fawn = Color(0xffe5aa70);
const Color Feldgrau = Color(0xff4d5d53);
const Color Fern = Color(0xff71bc78);
const Color FernGreen = Color(0xff4f7942);
const Color FerrariRed = Color(0xffff2800);
const Color FieldDrab = Color(0xff6c541e);
const Color FireEngineRed = Color(0xffce2029);
const Color Firebrick = Color(0xffb22222);
const Color Flame = Color(0xffe25822);
const Color FlamingoPink = Color(0xfffc8eac);
const Color Flavescent = Color(0xfff7e98e);
const Color Flax = Color(0xffeedc82);
const Color FloralWhite = Color(0xfffffaf0);
const Color FluorescentOrange = Color(0xffffbf00);
const Color FluorescentPink = Color(0xffff1493);
const Color FluorescentYellow = Color(0xffccff00);
const Color Folly = Color(0xffff004f);
const Color ForestGreen = Color(0xff228b22);
const Color FrenchBeige = Color(0xffa67b5b);
const Color FrenchBlue = Color(0xff0072bb);
const Color FrenchLilac = Color(0xff86608e);
const Color FrenchRose = Color(0xfff64a8a);
const Color Fuchsia = Color(0xffff00ff);
const Color FuchsiaPink = Color(0xffff77ff);
const Color Fulvous = Color(0xffe48400);
const Color FuzzyWuzzy = Color(0xffcc6666);
const Color Gainsboro = Color(0xffdcdcdc);
const Color Gamboge = Color(0xffe49b0f);
const Color GhostWhite = Color(0xfff8f8ff);
const Color Ginger = Color(0xffb06500);
const Color Glaucous = Color(0xff6082b6);
const Color Glitter = Color(0xffe6e8fa);
const Color Gold = Color(0xffffd700);
const Color GoldenBrown = Color(0xff996515);
const Color GoldenPoppy = Color(0xfffcc200);
const Color GoldenYellow = Color(0xffffdf00);
const Color Goldenrod = Color(0xffdaa520);
const Color GrannySmithApple = Color(0xffa8e4a0);
const Color Gray = Color(0xff808080);
const Color GrayAsparagus = Color(0xff465945);
const Color Green = Color(0xff00ff00);
const Color GreenBlue = Color(0xff1164b4);
const Color GreenYellow = Color(0xffadff2f);
const Color Grullo = Color(0xffa99a86);
const Color GuppieGreen = Color(0xff00ff7f);
const Color HalayaUbe = Color(0xff663854);
const Color HanBlue = Color(0xff446ccf);
const Color HanPurple = Color(0xff5218fa);
const Color HansaYellow = Color(0xffe9d66b);
const Color Harlequin = Color(0xff3fff00);
const Color HarvardCrimson = Color(0xffc90016);
const Color HarvestGold = Color(0xffda9100);
const Color HeartGold = Color(0xff808000);
const Color Heliotrope = Color(0xffdf73ff);
const Color HollywoodCerise = Color(0xfff400a1);
const Color Honeydew = Color(0xfff0fff0);
const Color HookerGreen = Color(0xff49796b);
const Color HotMagenta = Color(0xffff1dce);
const Color HotPink = Color(0xffff69b4);
const Color HunterGreen = Color(0xff355e3b);
const Color Icterine = Color(0xfffcf75e);
const Color Inchworm = Color(0xffb2ec5d);
const Color IndiaGreen = Color(0xff138808);
const Color IndianRed = Color(0xffcd5c5c);
const Color IndianYellow = Color(0xffe3a857);
const Color Indigo = Color(0xff4b0082);
const Color InternationalKleinBlue = Color(0xff002fa7);
const Color InternationalOrange = Color(0xffff4f00);
const Color Iris = Color(0xff5a4fcf);
const Color Isabelline = Color(0xfff4f0ec);
const Color IslamicGreen = Color(0xff009000);
const Color Ivory = Color(0xfffffff0);
const Color Jade = Color(0xff00a86b);
const Color Jasmine = Color(0xfff8de7e);
const Color Jasper = Color(0xffd73b3e);
const Color JazzberryJam = Color(0xffa50b5e);
const Color Jonquil = Color(0xfffada5e);
const Color JuneBud = Color(0xffbdda57);
const Color JungleGreen = Color(0xff29ab87);
const Color KuCrimson = Color(0xffe8000d);
const Color KellyGreen = Color(0xff4cbb17);
const Color Khaki = Color(0xffc3b091);
const Color LaSalleGreen = Color(0xff087830);
const Color LanguidLavender = Color(0xffd6cadd);
const Color LapisLazuli = Color(0xff26619c);
const Color LaserLemon = Color(0xfffefe22);
const Color LaurelGreen = Color(0xffa9ba9d);
const Color Lava = Color(0xffcf1020);
const Color Lavender = Color(0xffe6e6fa);
const Color LavenderBlue = Color(0xffccccff);
const Color LavenderBlush = Color(0xfffff0f5);
const Color LavenderGray = Color(0xffc4c3d0);
const Color LavenderIndigo = Color(0xff9457eb);
const Color LavenderMagenta = Color(0xffee82ee);
const Color LavenderMist = Color(0xffe6e6fa);
const Color LavenderPink = Color(0xfffbaed2);
const Color LavenderPurple = Color(0xff967bb6);
const Color LavenderRose = Color(0xfffba0e3);
const Color LawnGreen = Color(0xff7cfc00);
const Color Lemon = Color(0xfffff700);
const Color LemonYellow = Color(0xfffff44f);
const Color LemonChiffon = Color(0xfffffacd);
const Color LemonLime = Color(0xffbfff00);
const Color LightCrimson = Color(0xfff56991);
const Color LightThulianPink = Color(0xffe68fac);
const Color LightApricot = Color(0xfffdd5b1);
const Color LightBlue = Color(0xffadd8e6);
const Color LightBrown = Color(0xffb5651d);
const Color LightCarminePink = Color(0xffe66771);
const Color LightCoral = Color(0xfff08080);
const Color LightCornflowerBlue = Color(0xff93ccea);
const Color LightCyan = Color(0xffe0ffff);
const Color LightFuchsiaPink = Color(0xfff984ef);
const Color LightGoldenrodYellow = Color(0xfffafad2);
const Color LightGray = Color(0xffd3d3d3);
const Color LightGreen = Color(0xff90ee90);
const Color LightKhaki = Color(0xfff0e68c);
const Color LightPastelPurple = Color(0xffb19cd9);
const Color LightPink = Color(0xffffb6c1);
const Color LightSalmon = Color(0xffffa07a);
const Color LightSalmonPink = Color(0xffff9999);
const Color LightSeaGreen = Color(0xff20b2aa);
const Color LightSkyBlue = Color(0xff87cefa);
const Color LightSlateGray = Color(0xff778899);
const Color LightTaupe = Color(0xffb38b6d);
const Color LightYellow = Color(0xffffffed);
const Color Lilac = Color(0xffc8a2c8);
const Color Lime = Color(0xffbfff00);
const Color LimeGreen = Color(0xff32cd32);
const Color LincolnGreen = Color(0xff195905);
const Color Linen = Color(0xfffaf0e6);
const Color Lion = Color(0xffc19a6b);
const Color Liver = Color(0xff534b4f);
const Color Lust = Color(0xffe62020);
const Color MsuGreen = Color(0xff18453b);
const Color MacaroniAndCheese = Color(0xffffbd88);
const Color Magenta = Color(0xffff00ff);
const Color MagicMint = Color(0xffaaf0d1);
const Color Magnolia = Color(0xfff8f4ff);
const Color Mahogany = Color(0xffc04000);
const Color Maize = Color(0xfffbec5d);
const Color MajorelleBlue = Color(0xff6050dc);
const Color Malachite = Color(0xff0bda51);
const Color Manatee = Color(0xff979aaa);
const Color MangoTango = Color(0xffff8243);
const Color Mantis = Color(0xff74c365);
const Color Maroon = Color(0xff800000);
const Color Mauve = Color(0xffe0b0ff);
const Color MauveTaupe = Color(0xff915f6d);
const Color Mauvelous = Color(0xffef98aa);
const Color MayaBlue = Color(0xff73c2fb);
const Color MeatBrown = Color(0xffe5b73b);
const Color MediumPersianBlue = Color(0xff0067a5);
const Color MediumAquamarine = Color(0xff66ddaa);
const Color MediumBlue = Color(0xff0000cd);
const Color MediumCandyAppleRed = Color(0xffe2062c);
const Color MediumCarmine = Color(0xffaf4035);
const Color MediumChampagne = Color(0xfff3e5ab);
const Color MediumElectricBlue = Color(0xff035096);
const Color MediumJungleGreen = Color(0xff1c352d);
const Color MediumLavenderMagenta = Color(0xffdda0dd);
const Color MediumOrchid = Color(0xffba55d3);
const Color MediumPurple = Color(0xff9370db);
const Color MediumRedViolet = Color(0xffbb3385);
const Color MediumSeaGreen = Color(0xff3cb371);
const Color MediumSlateBlue = Color(0xff7b68ee);
const Color MediumSpringBud = Color(0xffc9dc87);
const Color MediumSpringGreen = Color(0xff00fa9a);
const Color MediumTaupe = Color(0xff674c47);
const Color MediumTealBlue = Color(0xff0054b4);
const Color MediumTurquoise = Color(0xff48d1cc);
const Color MediumVioletRed = Color(0xffc71585);
const Color Melon = Color(0xfffdbcb4);
const Color MidnightBlue = Color(0xff191970);
const Color MidnightGreen = Color(0xff004953);
const Color MikadoYellow = Color(0xffffc40c);
const Color Mint = Color(0xff3eb489);
const Color MintCream = Color(0xfff5fffa);
const Color MintGreen = Color(0xff98ff98);
const Color MistyRose = Color(0xffffe4e1);
const Color Moccasin = Color(0xfffaebd7);
const Color ModeBeige = Color(0xff967117);
const Color MoonstoneBlue = Color(0xff73a9c2);
const Color MordantRed19 = Color(0xffae0c00);
const Color MossGreen = Color(0xffaddfad);
const Color MountainMeadow = Color(0xff30ba8f);
const Color MountbattenPink = Color(0xff997a8d);
const Color Mulberry = Color(0xffc54b8c);
const Color Munsell = Color(0xfff2f3f4);
const Color Mustard = Color(0xffffdb58);
const Color Myrtle = Color(0xff21421e);
const Color NadeshikoPink = Color(0xfff6adc6);
const Color NapierGreen = Color(0xff2a8000);
const Color NaplesYellow = Color(0xfffada5e);
const Color NavajoWhite = Color(0xffffdead);
const Color NavyBlue = Color(0xff000080);
const Color NeonCarrot = Color(0xffffa343);
const Color NeonFuchsia = Color(0xfffe59c2);
const Color NeonGreen = Color(0xff39ff14);
const Color NonPhotoBlue = Color(0xffa4dded);
const Color NorthTexasGreen = Color(0xff059033);
const Color OceanBoatBlue = Color(0xff0077be);
const Color Ochre = Color(0xffcc7722);
const Color OfficeGreen = Color(0xff008000);
const Color OldGold = Color(0xffcfb53b);
const Color OldLace = Color(0xfffdf5e6);
const Color OldLavender = Color(0xff796878);
const Color OldMauve = Color(0xff673147);
const Color OldRose = Color(0xffc08081);
const Color Olive = Color(0xff808000);
const Color OliveDrab = Color(0xff6b8e23);
const Color OliveGreen = Color(0xffbab86c);
const Color Olivine = Color(0xff9ab973);
const Color Onyx = Color(0xff0f0f0f);
const Color OperaMauve = Color(0xffb784a7);
const Color Orange = Color(0xffffa500);
const Color OrangeYellow = Color(0xfff8d568);
const Color OrangePeel = Color(0xffff9f00);
const Color OrangeRed = Color(0xffff4500);
const Color Orchid = Color(0xffda70d6);
const Color OtterBrown = Color(0xff654321);
const Color OuterSpace = Color(0xff414a4c);
const Color OutrageousOrange = Color(0xffff6e4a);
const Color OxfordBlue = Color(0xff002147);
const Color PacificBlue = Color(0xff1ca9c9);
const Color PakistanGreen = Color(0xff006600);
const Color PalatinateBlue = Color(0xff273be2);
const Color PalatinatePurple = Color(0xff682860);
const Color PaleAqua = Color(0xffbcd4e6);
const Color PaleBlue = Color(0xffafeeee);
const Color PaleBrown = Color(0xff987654);
const Color PaleCarmine = Color(0xffaf4035);
const Color PaleCerulean = Color(0xff9bc4e2);
const Color PaleChestnut = Color(0xffddadaf);
const Color PaleCopper = Color(0xffda8a67);
const Color PaleCornflowerBlue = Color(0xffabcdef);
const Color PaleGold = Color(0xffe6be8a);
const Color PaleGoldenrod = Color(0xffeee8aa);
const Color PaleGreen = Color(0xff98fb98);
const Color PaleLavender = Color(0xffdcd0ff);
const Color PaleMagenta = Color(0xfff984e5);
const Color PalePink = Color(0xfffadadd);
const Color PalePlum = Color(0xffdda0dd);
const Color PaleRedViolet = Color(0xffdb7093);
const Color PaleRobinEggBlue = Color(0xff96ded1);
const Color PaleSilver = Color(0xffc9c0bb);
const Color PaleSpringBud = Color(0xffecebbd);
const Color PaleTaupe = Color(0xffbc987e);
const Color PaleVioletRed = Color(0xffdb7093);
const Color PansyPurple = Color(0xff78184a);
const Color PapayaWhip = Color(0xffffefd5);
const Color ParisGreen = Color(0xff50c878);
const Color PastelBlue = Color(0xffaec6cf);
const Color PastelBrown = Color(0xff836953);
const Color PastelGray = Color(0xffcfcfc4);
const Color PastelGreen = Color(0xff77dd77);
const Color PastelMagenta = Color(0xfff49ac2);
const Color PastelOrange = Color(0xffffb347);
const Color PastelPink = Color(0xffffd1dc);
const Color PastelPurple = Color(0xffb39eb5);
const Color PastelRed = Color(0xffff6961);
const Color PastelViolet = Color(0xffcb99c9);
const Color PastelYellow = Color(0xfffdfd96);
const Color Patriarch = Color(0xff800080);
const Color PayneGrey = Color(0xff536878);
const Color Peach = Color(0xffffe5b4);
const Color PeachPuff = Color(0xffffdab9);
const Color PeachYellow = Color(0xfffadfad);
const Color Pear = Color(0xffd1e231);
const Color Pearl = Color(0xffeae0c8);
const Color PearlAqua = Color(0xff88d8c0);
const Color Peridot = Color(0xffe6e200);
const Color Periwinkle = Color(0xffccccff);
const Color PersianBlue = Color(0xff1c39bb);
const Color PersianIndigo = Color(0xff32127a);
const Color PersianOrange = Color(0xffd99058);
const Color PersianPink = Color(0xfff77fbe);
const Color PersianPlum = Color(0xff701c1c);
const Color PersianRed = Color(0xffcc3333);
const Color PersianRose = Color(0xfffe28a2);
const Color Phlox = Color(0xffdf00ff);
const Color PhthaloBlue = Color(0xff000f89);
const Color PhthaloGreen = Color(0xff123524);
const Color PiggyPink = Color(0xfffddde6);
const Color PineGreen = Color(0xff01796f);
const Color Pink = Color(0xffffc0cb);
const Color PinkFlamingo = Color(0xfffc74fd);
const Color PinkSherbet = Color(0xfff78fa7);
const Color PinkPearl = Color(0xffe7accf);
const Color Pistachio = Color(0xff93c572);
const Color Platinum = Color(0xffe5e4e2);
const Color Plum = Color(0xffdda0dd);
const Color PortlandOrange = Color(0xffff5a36);
const Color PowderBlue = Color(0xffb0e0e6);
const Color PrincetonOrange = Color(0xffff8f00);
const Color PrussianBlue = Color(0xff003153);
const Color PsychedelicPurple = Color(0xffdf00ff);
const Color Puce = Color(0xffcc8899);
const Color Pumpkin = Color(0xffff7518);
const Color Purple = Color(0xff800080);
const Color PurpleHeart = Color(0xff69359c);
const Color PurpleMountainMajesty = Color(0xff9d81ba);
const Color PurpleMountainsMajesty = Color(0xff9678b6);
const Color PurplePizzazz = Color(0xfffe4eda);
const Color PurpleTaupe = Color(0xff50404d);
const Color Rackley = Color(0xff5d8aa8);
const Color RadicalRed = Color(0xffff355e);
const Color Raspberry = Color(0xffe30b5d);
const Color RaspberryGlace = Color(0xff915f6d);
const Color RaspberryPink = Color(0xffe25098);
const Color RaspberryRose = Color(0xffb3446c);
const Color RawSienna = Color(0xffd68a59);
const Color RazzleDazzleRose = Color(0xffff33cc);
const Color Razzmatazz = Color(0xffe3256b);
const Color Red = Color(0xffff0000);
const Color RedOrange = Color(0xffff5349);
const Color RedBrown = Color(0xffa52a2a);
const Color RedViolet = Color(0xffc71585);
const Color RichBlack = Color(0xff004040);
const Color RichCarmine = Color(0xffd70040);
const Color RichElectricBlue = Color(0xff0892d0);
const Color RichLilac = Color(0xffb666d2);
const Color RichMaroon = Color(0xffb03060);
const Color RifleGreen = Color(0xff414833);
const Color RobinsEggBlue = Color(0xff1fcecb);
const Color Rose = Color(0xffff007f);
const Color RoseBonbon = Color(0xfff9429e);
const Color RoseEbony = Color(0xff674846);
const Color RoseGold = Color(0xffb76e79);
const Color RoseMadder = Color(0xffe32636);
const Color RosePink = Color(0xffff66cc);
const Color RoseQuartz = Color(0xffaa98a9);
const Color RoseTaupe = Color(0xff905d5d);
const Color RoseVale = Color(0xffab4e52);
const Color Rosewood = Color(0xff65000b);
const Color RossoCorsa = Color(0xffd40000);
const Color RosyBrown = Color(0xffbc8f8f);
const Color RoyalAzure = Color(0xff0038a8);
const Color RoyalBlue = Color(0xff4169e1);
const Color RoyalFuchsia = Color(0xffca2c92);
const Color RoyalPurple = Color(0xff7851a9);
const Color Ruby = Color(0xffe0115f);
const Color Ruddy = Color(0xffff0028);
const Color RuddyBrown = Color(0xffbb6528);
const Color RuddyPink = Color(0xffe18e96);
const Color Rufous = Color(0xffa81c07);
const Color Russet = Color(0xff80461b);
const Color Rust = Color(0xffb7410e);
const Color SacramentoStateGreen = Color(0xff00563f);
const Color SaddleBrown = Color(0xff8b4513);
const Color SafetyOrange = Color(0xffff6700);
const Color Saffron = Color(0xfff4c430);
const Color SaintPatrickBlue = Color(0xff23297a);
const Color Salmon = Color(0xffff8c69);
const Color SalmonPink = Color(0xffff91a4);
const Color Sand = Color(0xffc2b280);
const Color SandDune = Color(0xff967117);
const Color Sandstorm = Color(0xffecd540);
const Color SandyBrown = Color(0xfff4a460);
const Color SandyTaupe = Color(0xff967117);
const Color SapGreen = Color(0xff507d2a);
const Color Sapphire = Color(0xff0f52ba);
const Color SatinSheenGold = Color(0xffcba135);
const Color Scarlet = Color(0xffff2400);
const Color SchoolBusYellow = Color(0xffffd800);
const Color ScreaminGreen = Color(0xff76ff7a);
const Color SeaBlue = Color(0xff006994);
const Color SeaGreen = Color(0xff2e8b57);
const Color SealBrown = Color(0xff321414);
const Color Seashell = Color(0xfffff5ee);
const Color SelectiveYellow = Color(0xffffba00);
const Color Sepia = Color(0xff704214);
const Color Shadow = Color(0xff8a795d);
const Color Shamrock = Color(0xff45cea2);
const Color ShamrockGreen = Color(0xff009e60);
const Color ShockingPink = Color(0xfffc0fc0);
const Color Sienna = Color(0xff882d17);
const Color Silver = Color(0xffc0c0c0);
const Color Sinopia = Color(0xffcb410b);
const Color Skobeloff = Color(0xff007474);
const Color SkyBlue = Color(0xff87ceeb);
const Color SkyMagenta = Color(0xffcf71af);
const Color SlateBlue = Color(0xff6a5acd);
const Color SlateGray = Color(0xff708090);
const Color Smalt = Color(0xff003399);
const Color SmokeyTopaz = Color(0xff933d41);
const Color SmokyBlack = Color(0xff100c08);
const Color Snow = Color(0xfffffafa);
const Color SpiroDiscoBall = Color(0xff0fc0fc);
const Color SpringBud = Color(0xffa7fc00);
const Color SpringGreen = Color(0xff00ff7f);
const Color SteelBlue = Color(0xff4682b4);
const Color StilDeGrainYellow = Color(0xfffada5e);
const Color Stizza = Color(0xff990000);
const Color Stormcloud = Color(0xff008080);
const Color Straw = Color(0xffe4d96f);
const Color Sunglow = Color(0xffffcc33);
const Color Sunset = Color(0xfffad6a5);
const Color SunsetOrange = Color(0xfffd5e53);
const Color Tan = Color(0xffd2b48c);
const Color Tangelo = Color(0xfff94d00);
const Color Tangerine = Color(0xfff28500);
const Color TangerineYellow = Color(0xffffcc00);
const Color Taupe = Color(0xff483c32);
const Color TaupeGray = Color(0xff8b8589);
const Color Tawny = Color(0xffcd5700);
const Color TeaGreen = Color(0xffd0f0c0);
const Color TeaRose = Color(0xfff4c2c2);
const Color Teal = Color(0xff008080);
const Color TealBlue = Color(0xff367588);
const Color TealGreen = Color(0xff006d5b);
const Color TerraCotta = Color(0xffe2725b);
const Color Thistle = Color(0xffd8bfd8);
const Color ThulianPink = Color(0xffde6fa1);
const Color TickleMePink = Color(0xfffc89ac);
const Color TiffanyBlue = Color(0xff0abab5);
const Color TigerEye = Color(0xffe08d3c);
const Color Timberwolf = Color(0xffdbd7d2);
const Color TitaniumYellow = Color(0xffeee600);
const Color Tomato = Color(0xffff6347);
const Color Toolbox = Color(0xff746cc0);
const Color Topaz = Color(0xffffc87c);
const Color TractorRed = Color(0xfffd0e35);
const Color TrolleyGrey = Color(0xff808080);
const Color TropicalRainForest = Color(0xff00755e);
const Color TrueBlue = Color(0xff0073cf);
const Color TuftsBlue = Color(0xff417dc1);
const Color Tumbleweed = Color(0xffdeaa88);
const Color TurkishRose = Color(0xffb57281);
const Color Turquoise = Color(0xff30d5c8);
const Color TurquoiseBlue = Color(0xff00ffef);
const Color TurquoiseGreen = Color(0xffa0d6b4);
const Color TuscanRed = Color(0xff66424d);
const Color TwilightLavender = Color(0xff8a496b);
const Color TyrianPurple = Color(0xff66023c);
const Color UaBlue = Color(0xff0033aa);
const Color UaRed = Color(0xffd9004c);
const Color UclaBlue = Color(0xff536895);
const Color UclaGold = Color(0xffffb300);
const Color UfoGreen = Color(0xff3cd070);
const Color UpForestGreen = Color(0xff014421);
const Color UpMaroon = Color(0xff7b1113);
const Color UscCardinal = Color(0xff990000);
const Color UscGold = Color(0xffffcc00);
const Color Ube = Color(0xff8878c3);
const Color UltraPink = Color(0xffff6fff);
const Color Ultramarine = Color(0xff120a8f);
const Color UltramarineBlue = Color(0xff4166f5);
const Color Umber = Color(0xff635147);
const Color UnitedNationsBlue = Color(0xff5b92e5);
const Color UniversityOfCaliforniaGold = Color(0xffb78727);
const Color UnmellowYellow = Color(0xffffff66);
const Color UpsdellRed = Color(0xffae2029);
const Color Urobilin = Color(0xffe1ad21);
const Color UtahCrimson = Color(0xffd3003f);
const Color Vanilla = Color(0xfff3e5ab);
const Color VegasGold = Color(0xffc5b358);
const Color VenetianRed = Color(0xffc80815);
const Color Verdigris = Color(0xff43b3ae);
const Color Vermilion = Color(0xffe34234);
const Color Veronica = Color(0xffa020f0);
const Color Violet = Color(0xffee82ee);
const Color VioletBlue = Color(0xff324ab2);
const Color VioletRed = Color(0xfff75394);
const Color Viridian = Color(0xff40826d);
const Color VividAuburn = Color(0xff922724);
const Color VividBurgundy = Color(0xff9f1d35);
const Color VividCerise = Color(0xffda1d81);
const Color VividTangerine = Color(0xffffa089);
const Color VividViolet = Color(0xff9f00ff);
const Color WarmBlack = Color(0xff004242);
const Color Waterspout = Color(0xff00ffff);
const Color Wenge = Color(0xff645452);
const Color Wheat = Color(0xfff5deb3);
const Color White = Color(0xffffffff);
const Color WhiteSmoke = Color(0xfff5f5f5);
const Color WildStrawberry = Color(0xffff43a4);
const Color WildWatermelon = Color(0xfffc6c85);
const Color WildBlueYonder = Color(0xffa2add0);
const Color Wine = Color(0xff722f37);
const Color Wisteria = Color(0xffc9a0dc);
const Color Xanadu = Color(0xff738678);
const Color YaleBlue = Color(0xff0f4d92);
const Color Yellow = Color(0xffffff00);
const Color YellowOrange = Color(0xffffae42);
const Color YellowGreen = Color(0xff9acd32);
const Color Zaffre = Color(0xff0014a8);
const Color ZinnwalditeBrown = Color(0xff2c1608);

//A huge list for you to traversal all colors above.
List<Color> colors = [
  AirForceBlue,
  AliceBlue,
  AlizarinCrimson,
  Almond,
  Amaranth,
  Amber,
  AmericanRose,
  Amethyst,
  AndroidGreen,
  AntiFlashWhite,
  AntiqueBrass,
  AntiqueFuchsia,
  AntiqueWhite,
  Ao,
  AppleGreen,
  Apricot,
  Aqua,
  Aquamarine,
  ArmyGreen,
  ArylideYellow,
  AshGrey,
  Asparagus,
  AtomicTangerine,
  Auburn,
  Aureolin,
  Aurometalsaurus,
  Awesome,
  Azure,
  AzureMist,
  BabyBlue,
  BabyBlueEyes,
  BabyPink,
  BallBlue,
  BananaMania,
  BananaYellow,
  BattleshipGrey,
  Bazaar,
  BeauBlue,
  Beaver,
  Beige,
  Bisque,
  Bistre,
  Bittersweet,
  Black,
  BlanchedAlmond,
  BleuDeFrance,
  BlizzardBlue,
  Blond,
  Blue,
  BlueBell,
  BlueGray,
  BlueGreen,
  BluePurple,
  BlueViolet,
  Blush,
  Bole,
  BondiBlue,
  Bone,
  BostonUniversityRed,
  BottleGreen,
  Boysenberry,
  BrandeisBlue,
  Brass,
  BrickRed,
  BrightCerulean,
  BrightGreen,
  BrightLavender,
  BrightMaroon,
  BrightPink,
  BrightTurquoise,
  BrightUbe,
  BrilliantLavender,
  BrilliantRose,
  BrinkPink,
  BritishRacingGreen,
  Bronze,
  Brown,
  BubbleGum,
  Bubbles,
  Buff,
  BulgarianRose,
  Burgundy,
  Burlywood,
  BurntOrange,
  BurntSienna,
  BurntUmber,
  Byzantine,
  Byzantium,
  CgBlue,
  CgRed,
  Cadet,
  CadetBlue,
  CadetGrey,
  CadmiumGreen,
  CadmiumOrange,
  CadmiumRed,
  CadmiumYellow,
  CafeAuLait,
  CafeNoir,
  CalPolyPomonaGreen,
  CambridgeBlue,
  Camel,
  CamouflageGreen,
  Canary,
  CanaryYellow,
  CandyAppleRed,
  CandyPink,
  Capri,
  CaputMortuum,
  Cardinal,
  CaribbeanGreen,
  Carmine,
  CarminePink,
  CarmineRed,
  CarnationPink,
  Carnelian,
  CarolinaBlue,
  CarrotOrange,
  Celadon,
  Celeste,
  CelestialBlue,
  Cerise,
  CerisePink,
  Cerulean,
  CeruleanBlue,
  Chamoisee,
  Champagne,
  Charcoal,
  Chartreuse,
  Cherry,
  CherryBlossomPink,
  Chestnut,
  Chocolate,
  ChromeYellow,
  Cinereous,
  Cinnabar,
  Cinnamon,
  Citrine,
  ClassicRose,
  Cobalt,
  CocoaBrown,
  Coffee,
  ColumbiaBlue,
  CoolBlack,
  CoolGrey,
  Copper,
  CopperRose,
  Coquelicot,
  Coral,
  CoralPink,
  CoralRed,
  Cordovan,
  Corn,
  CornellRed,
  Cornflower,
  CornflowerBlue,
  Cornsilk,
  CosmicLatte,
  CottonCandy,
  Cream,
  Crimson,
  CrimsonRed,
  CrimsonGlory,
  Cyan,
  Daffodil,
  Dandelion,
  DarkBlue,
  DarkBrown,
  DarkByzantium,
  DarkCandyAppleRed,
  DarkCerulean,
  DarkChestnut,
  DarkCoral,
  DarkCyan,
  DarkElectricBlue,
  DarkGoldenrod,
  DarkGray,
  DarkGreen,
  DarkJungleGreen,
  DarkKhaki,
  DarkLava,
  DarkLavender,
  DarkMagenta,
  DarkMidnightBlue,
  DarkOliveGreen,
  DarkOrange,
  DarkOrchid,
  DarkPastelBlue,
  DarkPastelGreen,
  DarkPastelPurple,
  DarkPastelRed,
  DarkPink,
  DarkPowderBlue,
  DarkRaspberry,
  DarkRed,
  DarkSalmon,
  DarkScarlet,
  DarkSeaGreen,
  DarkSienna,
  DarkSlateBlue,
  DarkSlateGray,
  DarkSpringGreen,
  DarkTan,
  DarkTangerine,
  DarkTaupe,
  DarkTerraCotta,
  DarkTurquoise,
  DarkViolet,
  DartmouthGreen,
  DavyGrey,
  DebianRed,
  DeepCarmine,
  DeepCarminePink,
  DeepCarrotOrange,
  DeepCerise,
  DeepChampagne,
  DeepChestnut,
  DeepCoffee,
  DeepFuchsia,
  DeepJungleGreen,
  DeepLilac,
  DeepMagenta,
  DeepPeach,
  DeepPink,
  DeepSaffron,
  DeepSkyBlue,
  Denim,
  Desert,
  DesertSand,
  DimGray,
  DodgerBlue,
  DogwoodRose,
  DollarBill,
  Drab,
  DukeBlue,
  EarthYellow,
  Ecru,
  Eggplant,
  Eggshell,
  EgyptianBlue,
  ElectricBlue,
  ElectricCrimson,
  ElectricCyan,
  ElectricGreen,
  ElectricIndigo,
  ElectricLavender,
  ElectricLime,
  ElectricPurple,
  ElectricUltramarine,
  ElectricViolet,
  ElectricYellow,
  Emerald,
  EtonBlue,
  Fallow,
  FaluRed,
  Famous,
  Fandango,
  FashionFuchsia,
  Fawn,
  Feldgrau,
  Fern,
  FernGreen,
  FerrariRed,
  FieldDrab,
  FireEngineRed,
  Firebrick,
  Flame,
  FlamingoPink,
  Flavescent,
  Flax,
  FloralWhite,
  FluorescentOrange,
  FluorescentPink,
  FluorescentYellow,
  Folly,
  ForestGreen,
  FrenchBeige,
  FrenchBlue,
  FrenchLilac,
  FrenchRose,
  Fuchsia,
  FuchsiaPink,
  Fulvous,
  FuzzyWuzzy,
  Gainsboro,
  Gamboge,
  GhostWhite,
  Ginger,
  Glaucous,
  Glitter,
  Gold,
  GoldenBrown,
  GoldenPoppy,
  GoldenYellow,
  Goldenrod,
  GrannySmithApple,
  Gray,
  GrayAsparagus,
  Green,
  GreenBlue,
  GreenYellow,
  Grullo,
  GuppieGreen,
  HalayaUbe,
  HanBlue,
  HanPurple,
  HansaYellow,
  Harlequin,
  HarvardCrimson,
  HarvestGold,
  HeartGold,
  Heliotrope,
  HollywoodCerise,
  Honeydew,
  HookerGreen,
  HotMagenta,
  HotPink,
  HunterGreen,
  Icterine,
  Inchworm,
  IndiaGreen,
  IndianRed,
  IndianYellow,
  Indigo,
  InternationalKleinBlue,
  InternationalOrange,
  Iris,
  Isabelline,
  IslamicGreen,
  Ivory,
  Jade,
  Jasmine,
  Jasper,
  JazzberryJam,
  Jonquil,
  JuneBud,
  JungleGreen,
  KuCrimson,
  KellyGreen,
  Khaki,
  LaSalleGreen,
  LanguidLavender,
  LapisLazuli,
  LaserLemon,
  LaurelGreen,
  Lava,
  Lavender,
  LavenderBlue,
  LavenderBlush,
  LavenderGray,
  LavenderIndigo,
  LavenderMagenta,
  LavenderMist,
  LavenderPink,
  LavenderPurple,
  LavenderRose,
  LawnGreen,
  Lemon,
  LemonYellow,
  LemonChiffon,
  LemonLime,
  LightCrimson,
  LightThulianPink,
  LightApricot,
  LightBlue,
  LightBrown,
  LightCarminePink,
  LightCoral,
  LightCornflowerBlue,
  LightCyan,
  LightFuchsiaPink,
  LightGoldenrodYellow,
  LightGray,
  LightGreen,
  LightKhaki,
  LightPastelPurple,
  LightPink,
  LightSalmon,
  LightSalmonPink,
  LightSeaGreen,
  LightSkyBlue,
  LightSlateGray,
  LightTaupe,
  LightYellow,
  Lilac,
  Lime,
  LimeGreen,
  LincolnGreen,
  Linen,
  Lion,
  Liver,
  Lust,
  MsuGreen,
  MacaroniAndCheese,
  Magenta,
  MagicMint,
  Magnolia,
  Mahogany,
  Maize,
  MajorelleBlue,
  Malachite,
  Manatee,
  MangoTango,
  Mantis,
  Maroon,
  Mauve,
  MauveTaupe,
  Mauvelous,
  MayaBlue,
  MeatBrown,
  MediumPersianBlue,
  MediumAquamarine,
  MediumBlue,
  MediumCandyAppleRed,
  MediumCarmine,
  MediumChampagne,
  MediumElectricBlue,
  MediumJungleGreen,
  MediumLavenderMagenta,
  MediumOrchid,
  MediumPurple,
  MediumRedViolet,
  MediumSeaGreen,
  MediumSlateBlue,
  MediumSpringBud,
  MediumSpringGreen,
  MediumTaupe,
  MediumTealBlue,
  MediumTurquoise,
  MediumVioletRed,
  Melon,
  MidnightBlue,
  MidnightGreen,
  MikadoYellow,
  Mint,
  MintCream,
  MintGreen,
  MistyRose,
  Moccasin,
  ModeBeige,
  MoonstoneBlue,
  MordantRed19,
  MossGreen,
  MountainMeadow,
  MountbattenPink,
  Mulberry,
  Munsell,
  Mustard,
  Myrtle,
  NadeshikoPink,
  NapierGreen,
  NaplesYellow,
  NavajoWhite,
  NavyBlue,
  NeonCarrot,
  NeonFuchsia,
  NeonGreen,
  NonPhotoBlue,
  NorthTexasGreen,
  OceanBoatBlue,
  Ochre,
  OfficeGreen,
  OldGold,
  OldLace,
  OldLavender,
  OldMauve,
  OldRose,
  Olive,
  OliveDrab,
  OliveGreen,
  Olivine,
  Onyx,
  OperaMauve,
  Orange,
  OrangeYellow,
  OrangePeel,
  OrangeRed,
  Orchid,
  OtterBrown,
  OuterSpace,
  OutrageousOrange,
  OxfordBlue,
  PacificBlue,
  PakistanGreen,
  PalatinateBlue,
  PalatinatePurple,
  PaleAqua,
  PaleBlue,
  PaleBrown,
  PaleCarmine,
  PaleCerulean,
  PaleChestnut,
  PaleCopper,
  PaleCornflowerBlue,
  PaleGold,
  PaleGoldenrod,
  PaleGreen,
  PaleLavender,
  PaleMagenta,
  PalePink,
  PalePlum,
  PaleRedViolet,
  PaleRobinEggBlue,
  PaleSilver,
  PaleSpringBud,
  PaleTaupe,
  PaleVioletRed,
  PansyPurple,
  PapayaWhip,
  ParisGreen,
  PastelBlue,
  PastelBrown,
  PastelGray,
  PastelGreen,
  PastelMagenta,
  PastelOrange,
  PastelPink,
  PastelPurple,
  PastelRed,
  PastelViolet,
  PastelYellow,
  Patriarch,
  PayneGrey,
  Peach,
  PeachPuff,
  PeachYellow,
  Pear,
  Pearl,
  PearlAqua,
  Peridot,
  Periwinkle,
  PersianBlue,
  PersianIndigo,
  PersianOrange,
  PersianPink,
  PersianPlum,
  PersianRed,
  PersianRose,
  Phlox,
  PhthaloBlue,
  PhthaloGreen,
  PiggyPink,
  PineGreen,
  Pink,
  PinkFlamingo,
  PinkSherbet,
  PinkPearl,
  Pistachio,
  Platinum,
  Plum,
  PortlandOrange,
  PowderBlue,
  PrincetonOrange,
  PrussianBlue,
  PsychedelicPurple,
  Puce,
  Pumpkin,
  Purple,
  PurpleHeart,
  PurpleMountainMajesty,
  PurpleMountainsMajesty,
  PurplePizzazz,
  PurpleTaupe,
  Rackley,
  RadicalRed,
  Raspberry,
  RaspberryGlace,
  RaspberryPink,
  RaspberryRose,
  RawSienna,
  RazzleDazzleRose,
  Razzmatazz,
  Red,
  RedOrange,
  RedBrown,
  RedViolet,
  RichBlack,
  RichCarmine,
  RichElectricBlue,
  RichLilac,
  RichMaroon,
  RifleGreen,
  RobinsEggBlue,
  Rose,
  RoseBonbon,
  RoseEbony,
  RoseGold,
  RoseMadder,
  RosePink,
  RoseQuartz,
  RoseTaupe,
  RoseVale,
  Rosewood,
  RossoCorsa,
  RosyBrown,
  RoyalAzure,
  RoyalBlue,
  RoyalFuchsia,
  RoyalPurple,
  Ruby,
  Ruddy,
  RuddyBrown,
  RuddyPink,
  Rufous,
  Russet,
  Rust,
  SacramentoStateGreen,
  SaddleBrown,
  SafetyOrange,
  Saffron,
  SaintPatrickBlue,
  Salmon,
  SalmonPink,
  Sand,
  SandDune,
  Sandstorm,
  SandyBrown,
  SandyTaupe,
  SapGreen,
  Sapphire,
  SatinSheenGold,
  Scarlet,
  SchoolBusYellow,
  ScreaminGreen,
  SeaBlue,
  SeaGreen,
  SealBrown,
  Seashell,
  SelectiveYellow,
  Sepia,
  Shadow,
  Shamrock,
  ShamrockGreen,
  ShockingPink,
  Sienna,
  Silver,
  Sinopia,
  Skobeloff,
  SkyBlue,
  SkyMagenta,
  SlateBlue,
  SlateGray,
  Smalt,
  SmokeyTopaz,
  SmokyBlack,
  Snow,
  SpiroDiscoBall,
  SpringBud,
  SpringGreen,
  SteelBlue,
  StilDeGrainYellow,
  Stizza,
  Stormcloud,
  Straw,
  Sunglow,
  Sunset,
  SunsetOrange,
  Tan,
  Tangelo,
  Tangerine,
  TangerineYellow,
  Taupe,
  TaupeGray,
  Tawny,
  TeaGreen,
  TeaRose,
  Teal,
  TealBlue,
  TealGreen,
  TerraCotta,
  Thistle,
  ThulianPink,
  TickleMePink,
  TiffanyBlue,
  TigerEye,
  Timberwolf,
  TitaniumYellow,
  Tomato,
  Toolbox,
  Topaz,
  TractorRed,
  TrolleyGrey,
  TropicalRainForest,
  TrueBlue,
  TuftsBlue,
  Tumbleweed,
  TurkishRose,
  Turquoise,
  TurquoiseBlue,
  TurquoiseGreen,
  TuscanRed,
  TwilightLavender,
  TyrianPurple,
  UaBlue,
  UaRed,
  UclaBlue,
  UclaGold,
  UfoGreen,
  UpForestGreen,
  UpMaroon,
  UscCardinal,
  UscGold,
  Ube,
  UltraPink,
  Ultramarine,
  UltramarineBlue,
  Umber,
  UnitedNationsBlue,
  UniversityOfCaliforniaGold,
  UnmellowYellow,
  UpsdellRed,
  Urobilin,
  UtahCrimson,
  Vanilla,
  VegasGold,
  VenetianRed,
  Verdigris,
  Vermilion,
  Veronica,
  Violet,
  VioletBlue,
  VioletRed,
  Viridian,
  VividAuburn,
  VividBurgundy,
  VividCerise,
  VividTangerine,
  VividViolet,
  WarmBlack,
  Waterspout,
  Wenge,
  Wheat,
  White,
  WhiteSmoke,
  WildStrawberry,
  WildWatermelon,
  WildBlueYonder,
  Wine,
  Wisteria,
  Xanadu,
  YaleBlue,
  Yellow,
  YellowOrange,
  YellowGreen,
  Zaffre,
  ZinnwalditeBrown
];
