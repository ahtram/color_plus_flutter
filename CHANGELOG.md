## [1.0.3] - 2023/10/22.

* Update to newest Flutter.

## [1.0.2] - 2020/07/23.

* Colors define are now const values.

## [1.0.1] - 2020/07/10.

* Add a get random color method.
* Update readme to fix the screenshot issue.

## [1.0.0] - 2020/07/09.

* First release. See the readme for good.
